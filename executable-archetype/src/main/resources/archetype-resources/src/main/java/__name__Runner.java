package ${package};

import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

/** Similar to a main method, this is the entry point for the executable. */
@Component
public class ${name}Runner implements ApplicationRunner {

  @Override
  public void run(ApplicationArguments args) {
    System.out.println("Hello World!");
  }
}
