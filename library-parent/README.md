# Java Library Parent POM #

This is the parent pom for all BYU HBLL java libraries. The main component that this POM provides is the
build process that produces javadocs and attaches sources to the jar. 

This POM can be referenced by adding the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>library-parent</artifactId>
  <version>5.2.1</version>
</parent>
```

A child project can be built by running the following command:

    mvn clean package

This will create a file with the name `{artifactId}-{version}.jar` in the target directory.