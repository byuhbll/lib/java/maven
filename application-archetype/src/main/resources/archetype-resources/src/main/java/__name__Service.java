package ${package};

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/** This is a web service demo and can be removed. */
@RestController
public class ${name}Service {

  @GetMapping(produces = "text/plain")
  public String get() {
    return "Hello, World!";
  }
}
