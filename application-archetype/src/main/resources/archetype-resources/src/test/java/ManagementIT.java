package ${package};

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

/** Integration tests for the management/actuator endpoints. */
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
public class ManagementIT {

  @Autowired private TestRestTemplate rest;

  /** Verifies the health endpoint is working. */
  @Test
  public void testHealth() {
    // Wrap in try-catch if needing to assert 4xx or 5xx responses.
    ResponseEntity<JsonNode> response = rest.getForEntity("/actuator/health", JsonNode.class);

    // Verify that the correct response code was sent.
    assertEquals(HttpStatus.OK, response.getStatusCode());

    // Verify that the correct response body was sent.
    JsonNode body = response.getBody();
    assertNotNull(body);
    assertEquals("UP", body.path("status").asText());
  }
}
