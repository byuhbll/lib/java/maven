# {REPLACE WITH PROJECT TITLE}

{REPLACE WITH PROJECT DESCRIPTION}

## Usage

This application is designed to be deployed via the GitLab pipelines to a Kubernetes environment. But to run it locally, execute:

```
docker run --rm -it registry.gitlab.com/byuhbll/apps/{PROJECT-SLUG}
```

### Configuration

{REPLACE WITH A LIST OF ENVIRONMENT VARIABLES FOR THIS APPLICATION}

## Developing This Project

Note: These instructions assume Docker and VS Code, with the Dev Containers extension, are installed.

Git clone the project locally and open it with VS Code. When prompted, reopen the project in a dev container. It will take a couple of minutes to download all of the dependencies and prepare the container for Java development. Proceed with development once the initialization is complete.

To run the application, create a launch.json file by clicking on the `Run and Debug` icon in VS Code and selecting `create a launch.json file`. If prompted, select `Java`. Add `"envFile": "${workspaceFolder}/.env"` to the main configuration in the newly created `launch.json` file. Create a file called `.env` in the project directory and add any necessary local configuration for running the application. Select `Run` -> `Start Debugging` to run the executable.

Tests can be run from within VS Code. Click the Testing icon and click "Run Tests".

To build and install the project locally for testing, run the following from the project directory on the host machine.

```
docker build -t myapp .

docker run --rm -it myapp
```

Building and deploying this project is handled by the GitLab pipeline.

## License

[License](LICENSE.md)